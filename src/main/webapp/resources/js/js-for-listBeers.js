function addBeer() {
	$.get("create", function(result) {

		$("#beerDialog").html(result);

		$('#beerDialog').dialog("option", "title", 'Add Beer');

		$("#beerDialog").dialog('open');

		//initializeDatePicker();
	});
	
	
}

function getLstBeerByCategoryId(id){
	//window.location.href = "http://localhost:8080/beersmanagement/seachByCategory/"+id;
	 $.ajax({
         type: 'GET',
         url: '/beersmanagement/seachByCategory',
         data: {
        	 term:id
         },
         success: function(content) {
            $('#myTable').html(content);  // replace
         }
     });
}

function editBeer(id) {

	$.get("get/" + id, function(result) {

		$("#beerDialog").html(result);

		$('#beerDialog').dialog("option", "title", 'Edit Beer');

		$("#beerDialog").dialog('open');

		//initializeDatePicker();
	});
}



function resetDialog(form) {

	form.find("input").val("");
}
var id="";
$(document).ready(function() {

	$('#beerDialog').dialog({

		autoOpen : false,
		position : 'center',
		modal : true,
		resizable : false,
		width : 440,
		buttons : {
			"Save" : function() {
				$('#beerForm').validate();
				 var isValid = $('#beerForm').valid();
				if(isValid){
					$('#beerForm').submit();
				}else{
					alert("Please input valid information before continuing");
				}
			},
			"Cancel" : function() {
				$(this).dialog('close');
			}
		},
		close : function() {

			resetDialog($('#beerForm'));

			$(this).dialog('close');
		}
	});

  
	
	 $( "#city" ).autocomplete({
		    source: function( request, response ) {
		      $.ajax({
		        url: "/beersmanagement/autocom",
		        dataType: "json",
		        data: {
		          q: request.term
		        },
		        success: function(data) {
                    response($.map(data, function(item) {
                    return {
                    	value: item.value,
                    	label: item.label
                    }
		             }));
		            }
		      });
		    },
		    minLength: 0,
		    select: function( event, ui ) {
		      id = ui.item.value;
		      $( "#city" ).val( ui.item.label );
		      getLstBeerByCategoryId(id);
		      return false;
		    },
		    
		    open: function() {
		      $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
		    },
		    close: function() {
		      $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
		    }
		  })
		  
		  $("#beerForm").validate({
				rules: {
					manufacturer: "required",
					name: "required",
					categoryId: "required",
					country: "required",
					price:  {
					      required: true,
					      number: true,
					      min:0
					    },
					description: "required"
					
				},
				messages: {
					manufacturer: "Please enter Manufacturer",
					name: "Please enter  name",
					categoryId: "Please enter category",
					country: "Please enter country",
					price: {
					      required: "Please enter price",
					      number: "Please enter number only",
					      min:"Price must be greater than 0"
					    },
					description: "Please enter description"
				}
			});
	  // auto complete combobox
	 
	
});








