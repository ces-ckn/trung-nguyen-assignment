<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<link rel="stylesheet"
	href='<c:url value="/resources/css/pure-0.4.2.css"/>'>

<link rel="stylesheet"
	href='<c:url value="/resources/css/font-awesome-4.0.3/css/font-awesome.css"/>'>


<style type="text/css">
th {
	text-align: left
}
</style>
	<div id="myTable">
		<table class="pure-table pure-table-bordered pure-table-striped" >
			<thead>
				<tr>
					<th width="12%">Manufacturer</th>
					<th width="12%">Name</th>
					<th width="12%">Category</th>
					<th width="12%">Country</th>
					<th width="12%">Price</th>
					<th width="12%">Description</th>
<!-- 					<th width="12%"></th> -->
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${beerList}" var="beer"  varStatus="loopCounter">
					<tr>
						<td><c:out value="${beer.manufacturer}" /></td>
						<td><c:out value="${beer.name}" /></td>
						<td><c:out value="${beer.categoryName}" /></td>
						<td><c:out value="${beer.country}" /></td>
						<td><c:out value="${beer.price}" /></td>
						<td><c:out value="${beer.description}" /></td>
						<td><nobr>
								<button class="pure-button pure-button-primary"
									onclick="editBeer(${beer.beerId});">

									<i class="fa fa-pencil"></i> Edit
								</button>

								<a class="pure-button pure-button-primary"
									onclick="return confirm('Are you sure you want to delete this beer?');"
									href="delete/${beer.beerId}"> <i class="fa fa-times"></i>Delete
								</a>

							</nobr></td>


					</tr>
				</c:forEach>
			</tbody>
		</table>
</div>

