<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<title>List Of Beers</title>

<link rel="stylesheet"
	href='<c:url value="/resources/css/pure-0.4.2.css"/>'>

<link rel="stylesheet"
	href='<c:url value="/resources/css/font-awesome-4.0.3/css/font-awesome.css"/>'>

<link rel="stylesheet"
	href='<c:url value="/resources/css/jquery.ui.all.css"/>'>
	
<link rel="stylesheet"
	href='<c:url value="/resources/css/jquery.dataTables.css"/>'>

<link rel="stylesheet"
	href='<c:url value="/resources/css/jquery-ui-1.10.4.custom.css"/>'>
	<link rel="stylesheet" href='<c:url value="/resources/css/bootstrap.css"/>'/>
    <link rel="stylesheet" href='<c:url value="/resources/css/bootstrapValidator.css"/>'/>		
  <style>
  .ui-autocomplete-loading {
    background: white url("<c:url value="/resources/css/images/ui-anim_basic_16x16.gif"/>") right center no-repeat;
  }

  </style>	

<style type="text/css">
th {
	text-align: left
}

#beerForm label.error {
		margin-left: 10px;
		width: auto;
		color: red;
		font: small-caps;
	}
	
  .custom-combobox {
    position: relative;
    display: inline-block;
  }
  .custom-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
  }
  .custom-combobox-input {
    margin: 0;
    padding: 5px 10px;
  }
</style>
<script type="text/javascript"
		src='<c:url value="/resources/js/lib/jquery-1.10.2.js"/>'></script>
	<script type="text/javascript"
		src='<c:url value="/resources/js/lib/jquery-ui-1.10.4.custom.js"/>'></script>
	<script type="text/javascript"
		src='<c:url value="/resources/js/lib/jquery.ui.datepicker.js"/>'></script>
	<script type="text/javascript"
		src='<c:url value="/resources/js/lib/jquery.ui.autocomplete.js"/>'></script>	
		<script type="text/javascript"
		src='<c:url value="/resources/js/lib/jquery.validate.js"/>'></script>
	
	<script type="text/javascript"
		src='<c:url value="/resources/js/lib/jquery.ui.core.js"/>'></script>
	<script type="text/javascript"
		src='<c:url value="/resources/js/lib/jquery.ui.button.js"/>'></script>
	<script type="text/javascript"
		src='<c:url value="/resources/js/lib/jquery.ui.position.js"/>'></script>
	<script type="text/javascript"
		src='<c:url value="/resources/js/lib/jquery.ui.menu.js"/>'></script>
	<script type="text/javascript"
		src='<c:url value="/resources/js/lib/jquery.ui.tooltip.js"/>'></script>
		
	<script type="text/javascript"
		src='<c:url value="/resources/js/js-for-listBeers.js"/>'></script>
		
	<script type="text/javascript"
		src='<c:url value="/resources/js/lib/jquery.dataTables.js"/>'></script>	

</head>




<body>
	<div style="width: 95%; margin: 0 auto;">

		<div id="beerDialog" style="display: none;">
			<%@ include file="beerForm.jsp"%>
		</div>

		<h1>List Of Beers</h1>

<div class="ui-widget">
  <label for="city">Category: </label>
  <input id="city">
</div>

		<button class="pure-button pure-button-primary" onclick="addBeer()">
			<i class="fa fa-plus"></i> Add Beer
		</button>
		<div id="myTable">
			<table class="pure-table pure-table-bordered pure-table-striped" >
			<thead>
				<tr>
					<th width="12%">Manufacturer</th>
					<th width="12%">Name</th>
					<th width="12%">Category</th>
					<th width="12%">Country</th>
					<th width="12%">Price</th>
					<th width="12%">Description</th>
<!-- 					<th width="12%"></th> -->
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${beerList}" var="beer"  varStatus="loopCounter">
					<tr>
					
						<td><c:out value="${beer.manufacturer}" /></td>
						<td><c:out value="${beer.name}" /></td>
						<td><c:out value="${beer.categoryName}" /></td>
						<td><c:out value="${beer.country}" /></td>
						<td><c:out value="${beer.price}" /></td>
						<td><c:out value="${beer.description}" /></td>
						<td><nobr>
								<button class="pure-button pure-button-primary"
									onclick="editBeer(${beer.beerId});">

									<i class="fa fa-pencil"></i> Edit
								</button>

								<a class="pure-button pure-button-primary"
									onclick="return confirm('Are you sure you want to delete this beer?');"
									href="delete/${beer.beerId}"> <i class="fa fa-times"></i>Delete
								</a>

							</nobr></td>


					</tr>
				</c:forEach>
			</tbody>
		</table>
		
		
		</div>
		
</div>


</body>
</html>