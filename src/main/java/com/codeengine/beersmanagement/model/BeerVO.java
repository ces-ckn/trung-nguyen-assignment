/**
 * 
 */
package com.codeengine.beersmanagement.model;

import java.io.Serializable;

/**
 * @author TrungNguyen
 *
 */
public class BeerVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String manufacturer;
	private String name;
	private String categoryId;
	private String categoryName;
	private String country;
	private Double price;
	private String description;
	private Boolean achiveFlg;
	private String beerId;
	/**
	 * @return the manufacturer
	 */
	public String getManufacturer() {
		return manufacturer;
	}
	/**
	 * @param manufacturer the manufacturer to set
	 */
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the categoryId
	 */
	public String getCategoryId() {
		return categoryId;
	}
	/**
	 * @param categoryId the categoryId to set
	 */
	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}
	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/**
	 * @return the price
	 */
	public Double getPrice() {
		return price;
	}
	/**
	 * @param price the price to set
	 */
	public void setPrice(Double price) {
		this.price = price;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the achiveFlg
	 */
	public Boolean getAchiveFlg() {
		return achiveFlg;
	}
	/**
	 * @param achiveFlg the achiveFlg to set
	 */
	public void setAchiveFlg(Boolean achiveFlg) {
		this.achiveFlg = achiveFlg;
	}
	
	/**
	 * @return the beerId
	 */
	public String getBeerId() {
		return beerId;
	}
	/**
	 * @param beerId the beerId to set
	 */
	public void setBeerId(String beerId) {
		this.beerId = beerId;
	}
	
	/**
	 * @return the categoryName
	 */
	public String getCategoryName() {
		return categoryName;
	}
	/**
	 * @param categoryName the categoryName to set
	 */
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BeerVO [manufacturer=" + manufacturer + ", name=" + name
				+ ", categoryId=" + categoryId + ", country=" + country
				+ ", price=" + price + ", description=" + description
				+ ", achiveFlg=" + achiveFlg + ", beerId=" + beerId + "]";
	}
	/**
	 * @param manufacturer
	 * @param name
	 * @param categoryId
	 * @param country
	 * @param price
	 * @param description
	 * @param achiveFlg
	 * @param beerId
	 */
	public BeerVO(String manufacturer, String name, String categoryId,
			String country, Double price, String description,
			Boolean achiveFlg, String beerId) {
		super();
		this.manufacturer = manufacturer;
		this.name = name;
		this.categoryId = categoryId;
		this.country = country;
		this.price = price;
		this.description = description;
		this.achiveFlg = achiveFlg;
		this.beerId = beerId;
	}
	/**
	 * 
	 */
	public BeerVO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	

}
