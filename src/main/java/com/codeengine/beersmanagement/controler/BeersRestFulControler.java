package com.codeengine.beersmanagement.controler;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.hibernate.classic.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.codeengine.beersmanagement.model.BaseVO;
import com.codeengine.beersmanagement.model.BeerVO;
import com.codeengine.beersmanagement.model.ListBeerWrapperVO;
import com.codeengine.beersmanagement.service.BeerService;
import com.codeengine.beersmanagement.service.CategoryService;
import com.codeengine.beersmanagement.util.BeerUtil;



/**
 * Handles requests for the application home page.
 */
@Controller
public class BeersRestFulControler {
	
	private static final Logger logger = LoggerFactory.getLogger(BeersRestFulControler.class);
	
	
	@Autowired
	private BeerService beerService; 
	
	@Autowired
	private CategoryService categoryService;
	public BeersRestFulControler() {
	    // pre-initialize the list of issuers available ...
	     
		  
	  }
		  
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}
	
	//TO-DO : get all list of beers
	@RequestMapping(value="/beers", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, BeerVO> getAllBeers(){
		logger.info("inside method getAllBeers");		
		return BeerUtil.beers;
	}
	
	//TO-DO : get all list of beers
	@RequestMapping(value="/autocom", method = RequestMethod.GET,produces="application/json")
	@ResponseBody
	public List<BaseVO> getBeerAutoComplete(@RequestParam(value = "q", required = false) String q){
		logger.info("inside method getBeerAutoComplete===="+q);	
		List<BaseVO> lst = new ArrayList<BaseVO>();
		BaseVO baseVO = null;
		for (Map.Entry<String,String> entry : BeerUtil.categories.entrySet()) {
			if(entry.getValue().indexOf(q) > -1 || entry.getKey().indexOf(q) > -1){
				baseVO = new BaseVO();
				baseVO.setLabel(entry.getValue());
				baseVO.setValue(entry.getKey());
				lst.add(baseVO);
			}
		} 
		return lst;
	}
	
	
	//TO-DO : get all list of beers
		@RequestMapping(value="/listBeers", method = RequestMethod.GET,produces="application/json")
		@ResponseBody
		public ListBeerWrapperVO getListBeer(){
			logger.info("inside method getListBeer====");	
			ListBeerWrapperVO  listBeerWrapperVO = new ListBeerWrapperVO();
			listBeerWrapperVO.setDraw(1);
			listBeerWrapperVO.setRecordsFiltered(BeerUtil.beers.size());
			listBeerWrapperVO.setRecordsTotal(BeerUtil.beers.size());
			List<BeerVO> lst = new ArrayList<BeerVO>(BeerUtil.beers.values()); 
			listBeerWrapperVO.setData(lst);
			return listBeerWrapperVO;
		}
	
	//TO-DO : add new beers
	@RequestMapping(value="/beers/addBeer", method=RequestMethod.POST)
    @ResponseBody
	public BeerVO addBeer(@ModelAttribute("beer") BeerVO beerVO) {
	     
	    if (beerVO != null) {
	      logger.info("Inside addBeer, adding: " + beerVO.toString());
	    } else {
	      logger.info("Inside addBeer... ");
	    }
	    BeerUtil.beers.put(beerVO.getBeerId(), beerVO);
	    
	    
	    return beerVO;
	  }
	
	//TO-DO: open new add new form
	
	 @RequestMapping(value="/beers/create", method=RequestMethod.GET)
	  public ModelAndView addIssuer() {		
	    return new ModelAndView("addBeer", "command", new BeerVO());
	  }
	
	
	//TO-DO : delete beers
	
	@RequestMapping(value="/beers/delete/{beerId}", method = RequestMethod.GET)
	@ResponseBody
	public BeerVO deleteBeerById(@PathVariable("beerId") String beerId){
		BeerVO beerVO = BeerUtil.beers.remove(beerId);
		if(null!=beerVO){
			logger.info("=beer=="+beerVO.toString());
		}else{
			logger.info("=There is no beer with id=="+beerId);
		}
		return beerVO;
	}
	
	//TO-DO : get beer by Id
	
	@RequestMapping(value="/beers/{beerId}", method = RequestMethod.GET)
	@ResponseBody
	public BeerVO getBeerById(@PathVariable("beerId") String beerId){
		BeerVO beerVO = BeerUtil.beers.get(beerId);
		if(null!=beerVO){
			logger.info("=beer=="+beerVO.toString());
		}else{
			logger.info("=There is no beer with id=="+beerId);
		}
		return beerVO;
	}
	
	
	
	
	
}
