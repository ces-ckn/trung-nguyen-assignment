/**
 * 
 */
package com.codeengine.beersmanagement.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="beer")

public class Beer  implements java.io.Serializable {


     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	 private Integer beerId;
     private Category category;
     private String name;
     private String description;
     private String country;
     private BigDecimal price;
     private String manufacturer;

    public Beer() {
    }

	
    public Beer(Category category, String name, String country, BigDecimal price, String manufacturer) {
        this.category = category;
        this.name = name;
        this.country = country;
        this.price = price;
        this.manufacturer = manufacturer;
    }
    public Beer(Category category, String name, String description, String country, BigDecimal price, String manufacturer) {
       this.category = category;
       this.name = name;
       this.description = description;
       this.country = country;
       this.price = price;
       this.manufacturer = manufacturer;
    }
   
     @Id @GeneratedValue(strategy=GenerationType.AUTO)

    
    @Column(name="beerId", unique=true, nullable=false)
    public Integer getBeerId() {
        return this.beerId;
    }
    
    public void setBeerId(Integer beerId) {
        this.beerId = beerId;
    }

@ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="catId", nullable=false)
    public Category getCategory() {
        return this.category;
    }
    
    public void setCategory(Category category) {
        this.category = category;
    }

    
    @Column(name="name", nullable=false)
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    
    @Column(name="description", length=10000)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    
    @Column(name="country", nullable=false)
    public String getCountry() {
        return this.country;
    }
    
    public void setCountry(String country) {
        this.country = country;
    }

    
    @Column(name="price", nullable=false, precision=18)
    public BigDecimal getPrice() {
        return this.price;
    }
    
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    
    @Column(name="manufacturer", nullable=false)
    public String getManufacturer() {
        return this.manufacturer;
    }
    
    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }




}