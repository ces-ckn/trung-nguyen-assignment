/**
 * 
 */
package com.codeengine.beersmanagement.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @author TrungPham
 *
 */
@Entity
@Table(name="category")
public class Category  implements java.io.Serializable {


     /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer catId;
    private String catName;
    private Set<Beer> beers = new HashSet<Beer>(0);

    public Category() {
    }

	
    public Category(String catName) {
        this.catName = catName;
    }
    public Category(String catName, Set<Beer> beers) {
       this.catName = catName;
       this.beers = beers;
    }
   
     @Id @GeneratedValue(strategy=GenerationType.AUTO)

    
    @Column(name="catId", unique=true, nullable=false)
    public Integer getCatId() {
        return this.catId;
    }
    
    public void setCatId(Integer catId) {
        this.catId = catId;
    }

    
    @Column(name="catName", nullable=false)
    public String getCatName() {
        return this.catName;
    }
    
    public void setCatName(String catName) {
        this.catName = catName;
    }

@OneToMany(fetch=FetchType.LAZY, mappedBy="category")
    public Set<Beer> getBeers() {
        return this.beers;
    }
    
    public void setBeers(Set<Beer> beers) {
        this.beers = beers;
    }

}
