package com.codeengine.beersmanagement.service.impl;

import java.util.List;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.codeengine.beersmanagement.dao.BeerDAO;
import com.codeengine.beersmanagement.entity.Beer;
import com.codeengine.beersmanagement.service.BeerService;
@Service("beerService")
public class BeerServiceImpl implements BeerService{

	@Autowired
	private BeerDAO beerDAO;
	
	@Override
	@Transactional
	public void saveBeer(Beer beer) {
		// TODO Auto-generated method stub
		beerDAO.saveBeer(beer);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Beer> listBeers() {
		// TODO Auto-generated method stub
		return beerDAO.listBeers();
	}

	@Override
	@Transactional(readOnly = true)
	public Beer getBeer(Long id) {
		// TODO Auto-generated method stub
		return beerDAO.getBeer(id);
	}

	@Override
	@Transactional
	public void deleteBeer(Long id) {
		// TODO Auto-generated method stub
		beerDAO.deleteBeer(id);
	}

}
