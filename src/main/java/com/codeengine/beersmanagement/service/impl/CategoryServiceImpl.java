/**
 * 
 */
package com.codeengine.beersmanagement.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.codeengine.beersmanagement.dao.CategoryDAO;
import com.codeengine.beersmanagement.entity.Category;
import com.codeengine.beersmanagement.service.CategoryService;

/**
 * @author TrungPham
 *
 */
@Service("categoryService")
public class CategoryServiceImpl implements CategoryService{

	@Autowired
	private CategoryDAO categoryDAO;
	@Override
	@Transactional
	public void saveCategory(Category category) {
		// TODO Auto-generated method stub
		System.out.println("BABABAB");
		categoryDAO.saveCategory(category);
	}

	@Override
	@Transactional(readOnly=true)
	public List<Category> listCategorys() {
		// TODO Auto-generated method stub
		return categoryDAO.listCategorys();
	}

	@Override
	@Transactional(readOnly=true)
	public Category getCategory(Long id) {
		// TODO Auto-generated method stub
		return categoryDAO.getCategory(id);
	}

	@Override
	@Transactional
	public void deleteCategory(Long id) {
		// TODO Auto-generated method stub
		categoryDAO.deleteCategory(id);
	}

}
