/**
 * 
 */
package com.codeengine.beersmanagement.dao;

import java.util.List;

import com.codeengine.beersmanagement.entity.Beer;



/**
 * @author TrungNguyen
 *
 */
public interface BeerDAO {
	/*
	 * CREATE and UPDATE
	 */
	public void saveBeer(Beer beer); // create and update

	/*
	 * READ
	 */
	public List<Beer> listBeers();
	public Beer getBeer(Long id);

	/*
	 * DELETE
	 */
	public void deleteBeer(Long id);
}
