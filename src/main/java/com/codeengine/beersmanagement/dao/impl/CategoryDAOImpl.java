/**
 * 
 */
package com.codeengine.beersmanagement.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import com.codeengine.beersmanagement.dao.CategoryDAO;
import com.codeengine.beersmanagement.entity.Category;

/**
 * @author TrungPham
 *
 */
@Repository("categoryDAO")
public class CategoryDAOImpl implements CategoryDAO{

	private Log log = LogFactory.getLog(BeerDAOImpl.class);
	
	private SessionFactory sessionFactory;	

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Resource(name="sessionFactory")
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
	@Override
	public void saveCategory(Category category) {
		// TODO Auto-generated method stub
		System.out.println("===saveCategory=======");
		this.getSession().saveOrUpdate(category);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Category> listCategorys() {
		// TODO Auto-generated method stub
		return this.getSession().createQuery("from Category c").list();
	}

	@Override
	public Category getCategory(Long id) {
		// TODO Auto-generated method stub
		return (Category)this.getSession().get(Category.class, id);
	}

	@Override
	public void deleteCategory(Long id) {
		// TODO Auto-generated method stub
		Category category = this.getCategory(id);
		this.getSession().delete(category);
	}

	private Session getSession() {
		Session sess = getSessionFactory().getCurrentSession();
		if (sess == null) {
			sess = getSessionFactory().openSession();
		}
		return sess;
	}
}
