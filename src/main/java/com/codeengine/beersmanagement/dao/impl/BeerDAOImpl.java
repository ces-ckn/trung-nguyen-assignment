/**
 * 
 */
package com.codeengine.beersmanagement.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;






import com.codeengine.beersmanagement.dao.BeerDAO;
import com.codeengine.beersmanagement.entity.Beer;

/**
 * @author TrungPham
 *
 */
@Repository("beerDAO")
public class BeerDAOImpl implements BeerDAO {

	private Log log = LogFactory.getLog(BeerDAOImpl.class);
	@Autowired
	private SessionFactory sessionFactory;	
	
	@Override
	public void saveBeer(Beer beer) {
		// TODO Auto-generated method stub
		getSession().merge(beer);
		
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Beer> listBeers() {
		// TODO Auto-generated method stub
		log.info("======listBeers======");
		return this.getSession().createCriteria(Beer.class).list();
	}

	@Override
	public Beer getBeer(Long id) {
		// TODO Auto-generated method stub
		return (Beer)this.getSession().get(Beer.class, id);
	}

	@Override
	public void deleteBeer(Long id) {
		// TODO Auto-generated method stub
		Beer beer = getBeer(id);

		if (null != beer) {
			getSession().delete(beer);
		}	

	}

	private Session getSession() {
		Session sess = sessionFactory.getCurrentSession();
		if (sess == null) {
			sess = sessionFactory.openSession();
		}
		return sess;
	}
}
